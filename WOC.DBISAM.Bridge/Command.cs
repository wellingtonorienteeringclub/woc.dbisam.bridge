﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace WOC.DBISAM.Bridge {
  /// <summary>
  /// Class to interact with the database.
  /// </summary>
  public class Commander {
    [DllImport("WOC.DBISAM.64.dll", CharSet = CharSet.Unicode)]
    internal static extern bool ExportTable(string path, string table, string columns, string tmpFile);

    [DllImport("WOC.DBISAM.64.dll", CharSet = CharSet.Unicode)]
    internal static extern bool UpdateTable(string path, string query);

    /// <summary>
    /// Preforms a select query for a table.
    /// </summary>
    /// <param name="path">The path to folder that has contains the table.</param>
    /// <param name="table">The name of the table to get the data from.</param>
    /// <param name="columns">The table columns to get the data for.</param>
    /// <returns></returns>
    public string GetTableData(string path, string table, List<string> columns) {
      string tmpFile = Path.GetTempPath() + Guid.NewGuid().ToString() + ".csv";

      bool retval = ExportTable(path, table, String.Join(",", columns), tmpFile);
      if (!retval) {
        return null;
      }
      var content = File.ReadAllText(tmpFile);
      File.Delete(tmpFile);
      return content;
    }

    /// <summary>
    /// Executes a sql query
    /// </summary>
    /// <param name="path">The path to folder that has contains the table.</param>
    /// <param name="query">The query to execute.</param>
    /// <returns>True if query executed successfully.</returns>
    public bool ExecuteSql(string path, string query) {
      return UpdateTable(path, query);
    }
  }
}
